package de.fu_berlin.agdb.swiss_tax.model;

/**
 * Cantons of Switzerland. Administrative division of the Swiss Confederation. Similar to German Bundesland.
 */
public enum Canton {

    AARGAU("Aargau", "AG"),
    APPENZELL_INNERRHODEN("Appenzell Innerrhoden", "AI"),
    APPENZELL_AUSSERRHODEN("Appenzell Ausserrhoden", "AR"),
    BERN("Bern", "BE"),
    BASEL_LANDSCHAFT("Basel-Landschaft", "BL"),
    BASEL_STADT("Basel-Stadt", "BS"),
    FRIBOURG("Fribourg", "FR"),
    GENEVA("Geneva", "GE"),
    GLARUS("Glarus", "GL"),
    GRAUBÜNDEN("Graubünden", "GR"),
    JURA("Jura", "JU"),
    LUCERNE("Lucerne", "LU"),
    NEUCHÂTEL("Neuchâtel", "NE"),
    NIDWALDEN("Nidwalden", "NW"),
    OBWALDEN("Obwalden", "OW"),
    ST_GALLEN("St. Gallen", "SG"),
    SCHAFFHAUSEN("Schaffhausen", "SH"),
    SOLOTHURN("Solothurn", "SO"),
    SCHWYZ("Schwyz", "SZ"),
    THURGAU("Thurgau", "TG"),
    TICINO("Ticino", "TI"),
    URI("Uri", "UR"),
    VAUD("Vaud", "VD"),
    VALAIS("Valais", "VS"),
    ZUG("Zug", "ZG"),
    ZURICH("Zurich", "ZH"),
    NOT_SPECIFIED("nicht angegeben", null);

    private String code;
    private String name;

    Canton(String name, String code) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    /**
     * get swiss canton based on code.
     *
     * @param code code of marital status
     * @return one of marital status enums
     */
    public static Canton fromCode(String code) {
        for (Canton canton : Canton.values()) {
            if (canton.getCode().equals(code)) {
                return canton;
            }
        }
        return Canton.NOT_SPECIFIED;
    }

}
