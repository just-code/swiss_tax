package de.fu_berlin.agdb.swiss_tax.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

/**
 * Different web security configurations for (supervising) hygienist, veterinarian and animal keeper authorities.
 */
@EnableWebSecurity
public class SecurityConfiguration {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // setup jdbc authentication with user email and password
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder())
                .usersByUsernameQuery(
                        "SELECT u.name, u.password, u.enabled " +
                                "FROM account AS u " +
                                "WHERE u.name = ?"
                )
                .authoritiesByUsernameQuery(
                        "SELECT u.name, a.name " +
                                "FROM account AS u " +
                                "INNER JOIN authority AS a ON u.id = a.account_id " +
                                "WHERE u.name = ?"
                );
    }

    @Autowired
    private DataSource dataSource;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    /**
     * Spring security for REST
     */
    @Configuration
    public static class LoginSecurityConfig extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // TODO: implement some basic authentication scheme
            http.authorizeRequests().anyRequest()
                    .permitAll();
        }
    }

}
